# Why
When we develop applications we need to have a straight forward way to create new module, we need to structure them and define their interaction.

The result of introduction of architecture design should reduce complexity of the project by establishing clear interaction between and within application modules.


Architecture design pattern should fit the environment of development and team, there is no best architecture for everything and everyone.

`QML` applications usually lack of architecture, because of simple way to prototype application with this flexible declarative language. Components becomes bulky after a time, when more and more features introduced.

To solve this issue we need to define good architecture for `QML` application, where declarative root will stay positive without negatives of hard-coupled and bulky components.

# Architectures
## MVC
**Model - View - Controller** is least fitting architectures for the `QML`. It's hard coupled architecture forces View to interact with Model and user input handled by Controller. 

## MVP
**Model - View - Presenter** has advantages that `View` interacts only with `Presenter` where the last one prepares data to shown for `View`. But `View` and `Presenter` is hard coupled which is not possible if we think `QML` as a `View`.

Advantages:
- Easy to cover by tests.
- Doesn't spread business logic across all of components.

Disadvantages:
- Tight interaction between `View` and `Presenter` creates less generalized code and forces `View` to manage states.
- Too much logic inside `Presenter`, which contains business logic, manages to interact with UI and contains interaction between application modules.

## MVVM
**Model - View - ViewModel** is improved `MVP` architecture. By decoupling `View` and `Presenter`, with notification instead of calls, it is possible to apply this architecture in a nice way and natural in `QML`.

`Model` is a basic entity or data model which contains the data obtained from raw data such as SQL Database.

`View` handled user input and passes it to the `ViewModel`, display data from `ViewModel`.

`ViewModel` processes user input from the `View`, processes data from `Model` to display and most important - contains business logic.

Advantages:
- Decoupled reactive components.
- Easy to cover by tests.
- Doesn't spread business logic across all of components.

Disadvantages:
- `ViewModel` still should be binded in some way with the `View`. This causes business logic still hard coupled with the UI framework.
- Heavy logic `ViewModel` contains: business logic, cross-modules interaction, view data preparation.

## VIPER
**View - Interactor - Presenter - Entity - Routing** it is most complex architecture, which has a new level of decoupling. 

**View** - displays the data from `Presenter`, handles user input and passes to the `Presenter`

**Presenter** - prepares content for the `View` from data received from the `Interactor`. Handles user input and request data changes from `Interactor`.

**Interactor** - contains business logic, manages with data provided by `Entity`.

**Entity** - model data for the `Interactor` can be proder of data from backend or from other modules

**Routing** - navigation logic in application, provides `Presenter` a way to show screens and dialogs.

Advantages:
- Single responsibility components.
- Easy to cover with tests.
- Reusable and replacable components.
- Cross interaction of modules can be covered with tests.

Disadvantages:
- A lot of boilerplate code for simple modules.
- Can be hard to implement new modules for unexperienced in application logic users. 


```plantuml
@startuml
package "Current Module" {
package qml <<Frame>> {
class View
}
package qobject <<Frame>> {
class Presenter
class Routing
}
package cpp_object <<Frame>> {
class Interactor
class Entity
}
}
package "External Module" {
class ExternalEntity
}
package "Input Module" {
class InputInteractor
}
View o--* Presenter
Presenter o--* Interactor
Presenter -right-o Routing
Interactor --o Entity
Interactor o-* "1.." ExternalEntity
InputInteractor o--* "1.." Entity
@enduml
```